<!DOCTYPE html>
<html>
    <head>
        <meta charset ="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Kuvagalleria</title>
    </head>
    <body>
        <h3>Tallentaa tiedoston</h3>

        <?php
        if ($_FILES['tiedot']['error'] == UPLOAD_ERR_OK){
             if($_FILES['tiedot']['size'] > 0) {
                 $tyyppi = $_FILES['tiedot']['type']; 
                 if (strcmp($tyyppi, "application/pdf") == 0) {
                     $file = basename($_FILES['tiedot']['name']);
                     $folder = 'uploads/';

                     if (move_uploaded_file($_FILES['tiedot']['tmp_name'], "$folder$file")) {
                         print "<p>Tiedosto tallentui.</p>";
                         print "<a href='index.php'>Selaa kuvia.</a>";
                     } else{
                         print "<p>Tapahtui virhe.</p>";
                     }
                 } else{
                     print "<p>Vain pdf-tiedostot sallittu.</p>";
                 }
             }
        }
        ?>
    </body>